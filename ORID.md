**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview：没有按要求拆分mapper，存在未引用的变量与函数
2. cloud Natice：学习了云原生的基本概念，与上星期的session容器，微服务进行关联
3. springboot concept map：小组之间没有像其他组活跃，希望能在下次改进吧
4. Html  Css：复习了两个前端基础技术栈，为下午的react做准备
5. react组件划分：组件化主要遵循两个原则：独立性和可用性
6. JSX：用于react的模板语言
7. react组件间交互：主要用了props与state

**R (Reflective): Please use one word to express your feelings about today's class.**

新奇

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

react与vue感觉还是有些互通，可能还是刚接触react，感觉有点生疏，这周要好好掌握这门技术栈

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

多去练习