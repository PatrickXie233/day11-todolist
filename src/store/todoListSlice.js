import { createSlice } from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
    name: 'todoListSlice',
    initialState: {
        todoList: [],
    },
    reducers: {
        addTodoItem: (state, { payload }) => {
            state.todoList.push({
                id: Math.ceil(Math.random() * 1000000),
                text: payload,
                done: false,
            })
        },
        doneTodoItem: (state, { payload }) => {
            state.todoList[payload].done = !state.todoList[payload].done
        },
        deleteTodoItem: (state, { payload }) => {
            console.log(payload)
            state.todoList = state.todoList.filter(
                (item, index) => index !== payload,
            )
        },
    },
})

// Action creators are generated for each case reducer function
export const { addTodoItem, doneTodoItem, deleteTodoItem } =
    todoListSlice.actions

export default todoListSlice.reducer
