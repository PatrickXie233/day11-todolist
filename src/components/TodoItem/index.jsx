import './index.css'
import { useDispatch } from 'react-redux'
import { doneTodoItem,deleteTodoItem } from '../../store/todoListSlice'
const TodoItem = ({ value, index, done }) => {
    const dispatch = useDispatch()
    const handleClick = () => {
        dispatch(doneTodoItem(index))
    }
    const handleDelete = () => {
        dispatch(deleteTodoItem(index))
    }

    return (
        <div className="todo-item-container">
            <div className="todo-item" onClick={handleClick}>
                <div
                    className={
                        done ? 'todo-item-text line-through' : 'todo-item-text'
                    }
                >
                    {value}
                </div>
            </div>
            <button className="todo-item-close" onClick={handleDelete}>
                X
            </button>
        </div>
    )
}
export default TodoItem
