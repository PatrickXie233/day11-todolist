import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addTodoItem } from '../../store/todoListSlice'
import './index.css'
const TodoGenerator = () => {
    const [inputValue, setInputValue] = useState('')
    const dispatch = useDispatch()
    const onClick = () => {
        if (inputValue.length === 0) {
            alert('请输入内容')
            return
        }
        dispatch(addTodoItem(inputValue))
        setInputValue('')
    }
    const onChange = (e) => {
        setInputValue(e.target.value)
    }
    return (
        <div className="todoGenerator">
            <input
                type="text"
                value={inputValue}
                onChange={onChange}
                placeholder="请输入Todo"
            ></input>
            <button onClick={onClick}>add</button>
        </div>
    )
}

export default TodoGenerator
