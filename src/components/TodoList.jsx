import TodoGenerator from './TodoGenerator'
import TodoGroup from './TodoGroup'
const TodoList = () => {
    return (
        <div className="container">
            <h2> Todo List </h2>
            <TodoGroup></TodoGroup>
            <TodoGenerator></TodoGenerator>
        </div>
    )
}
export default TodoList
