import { useSelector } from 'react-redux'
import TodoItem from './TodoItem/index.jsx'

const TodoGroup = (props) => {
    const todoList = useSelector((state) => state.todoList.todoList)
    return (
        <div className="todoGroup">
            {todoList.map((item, index) => {
                return (
                    <TodoItem
                        value={item.text}
                        key={item.id}
                        index={index}
                        done={item.done}
                    ></TodoItem>
                )
            })}
        </div>
    )
}
export default TodoGroup
